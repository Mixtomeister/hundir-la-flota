package launcher;

import model.Config;
import model.ServerAdmin;

public class Launcher {
	public static void main(String[] args) {
		Config config = new Config("data/config.json");
		ServerAdmin server = new ServerAdmin(config.get("ip"), Integer.parseInt(config.get("gate")));
		System.out.println("Server started - Battleship");
		server.start();
	}
}
