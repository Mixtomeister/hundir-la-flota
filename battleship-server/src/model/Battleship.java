package model;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Battleship extends Thread {
	private Player p1;
	private Player p2;

	private boolean inGame;

	public Battleship(Socket sc1, Socket sc2) {
		this.p1 = new Player(sc1);
		this.p2 = new Player(sc2);
	}

	@Override
	public void run() {
		System.out.println("Started: Game " + getId());
		notifyStart();
		getNicknames();
		System.out.println("Game " + getId() + ": " + p1.getNickname() + " vs " + p2.getNickname());
		generarMapas();
		enviarMapas();
		inGame = true;
		while (inGame) {
			turn(p1, p2);
			if(inGame) {
				turn(p2, p1);
			}
		}
		if (p1.getAciertos() == 15) {
			sendWin(p1, 0);
			sendWin(p2, 1);
		}else {
			sendWin(p1, 1);
			sendWin(p2, 0);
		}
		System.out.println("Finished: Game " + getId());
	}

	private void notifyStart() {
		Gson gson = new GsonBuilder().create();
		DataInfo data = new DataInfo(0);
		data.set("request", "start");
		data.set("client", "1");
		String req = gson.toJson(data);
		send(p1, req);
		data.set("client", "2");
		req = gson.toJson(data);
		send(p2, req);
	}

	private void getNicknames() {
		Gson gson = new GsonBuilder().create();
		String temp = listen(p1);
		DataInfo data = gson.fromJson(temp.substring(0, temp.indexOf("}}") + 2), DataInfo.class);
		p1.setNickname(data.get("nick"));
		temp = listen(p2);
		data = gson.fromJson(temp.substring(0, temp.indexOf("}}") + 2), DataInfo.class);
		p2.setNickname(data.get("nick"));
	}

	private void generarMapas() {
		int barcos[] = { 5, 4, 3, 2, 1 };
		MapGenerator map1 = new MapGenerator(barcos);
		MapGenerator map2 = new MapGenerator(barcos);

		p1.setMap(map1.generate());
		p2.setMap(map2.generate());
	}

	private void enviarMapas() {
		Gson gson = new GsonBuilder().create();
		DataInfo data = new DataInfo(0);
		data.set("request", "map");
		data.set("map", gson.toJson(p1.getMap()));
		String req = gson.toJson(data);
		send(p1, req);
		data.set("map", gson.toJson(p2.getMap()));
		req = gson.toJson(data);
		send(p2, req);
	}

	private void turn(Player player, Player target) {
		giveTurn(player);
		Disparo dis = waitShoot(player);
		int tar = target.disparo(dis.getX(), dis.getY());
		sendTarget(player, tar);
		sendShootme(target, dis.getX(), dis.getY(), tar);
	}

	private void giveTurn(Player player) {
		Gson gson = new GsonBuilder().create();
		DataInfo data = new DataInfo(0);
		data.set("request", "turn");
		String req = gson.toJson(data);
		send(player, req);
	}

	private Disparo waitShoot(Player player) {
		Gson gson = new GsonBuilder().create();
		String temp = listen(player);
		DataInfo data = gson.fromJson(temp.substring(0, temp.indexOf("}}") + 2), DataInfo.class);
		return new Disparo(Integer.parseInt(data.get("x")), Integer.parseInt(data.get("y")));
	}

	private void sendTarget(Player player, int target) {
		Gson gson = new GsonBuilder().create();
		DataInfo data = new DataInfo(0);
		data.set("request", "shoot");
		data.set("target", String.valueOf(target));
		String req = gson.toJson(data);
		send(player, req);
		if (player.getAciertos() == 15) {
			inGame = false;
		}
	}
	
	private void sendShootme(Player player, int x, int y, int target) {
		Gson gson = new GsonBuilder().create();
		DataInfo data = new DataInfo(0);
		data.set("request", "shootme");
		data.set("target", String.valueOf(target));
		data.set("x", String.valueOf(x));
		data.set("y", String.valueOf(y));
		String req = gson.toJson(data);
		send(player, req);
	}
	
	private void sendWin(Player player, int win) {
		Gson gson = new GsonBuilder().create();
		DataInfo data = new DataInfo(0);
		data.set("request", "win");
		data.set("win", String.valueOf(win));
		String req = gson.toJson(data);
		send(player, req);
	}

	private void send(Player player, String msg) {
		try {
			player.getSc().getOutputStream().write(msg.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String listen(Player player) {
		char[] msg = new char[1000];
		String line = "";
		try {
			new InputStreamReader(player.getSc().getInputStream()).read(msg);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new String(msg);
	}
}
