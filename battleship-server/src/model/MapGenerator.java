package model;

public class MapGenerator {
	private int map[][];
	private int barcos[];
	
	public MapGenerator(int barcos[]) {
		this.barcos = barcos;
	}
	
	public int[][] generate(){
		map = new int[10][10];
		for (int i = 0; i < barcos.length; i++) {
			colocarBarco(barcos[i]);
		}
		return map;
	}
	
	private void colocarBarco(int barco) {
		boolean flag = false;
		while(!flag) {
			flag = true;
			int x = (int) Math.floor(Math.random()*(0-9+1)+9);
			int y = (int) Math.floor(Math.random()*(0-9+1)+9);
			if(this.map[x][y] == 0) {
				if(x % 2 == 0) {
					while((x + barco) > 9) {
						x = (int) Math.floor(Math.random()*(0-9+1)+9);
					}
					for (int i = 0; i < barco && flag; i++) {
						if(this.map[x + i][y] != 0) {
							flag = false;
						}
					}
					for (int i = 0; i < barco && flag; i++) {
						this.map[x + i][y] = barco;
					}
				}else {
					while((y + barco) > 9) {
						y = (int) Math.floor(Math.random()*(0-9+1)+9);
					}
					for (int i = 0; i < barco && flag; i++) {
						if(this.map[x][y + i] != 0) {
							flag = false;
						}
					}
					for (int i = 0; i < barco && flag; i++) {
						this.map[x][y + i] = barco;
					}
				}
			}else {
				flag = false;
			}
		}
	}
}
