package model;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerAdmin {
	private ServerSocket serverSocket;
	
	private String ipServer;
	private int gateServer;
	
	public ServerAdmin(String ip, int gate) {
		this.ipServer = ip;
		this.gateServer = gate;
		try {
			this.serverSocket = new ServerSocket();
			this.serverSocket.bind(new InetSocketAddress(this.gateServer));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void start() {
		while(true) {
			try {
				Socket sc1 = this.serverSocket.accept();
				System.out.println("Cliente 1 accepted");
				Socket sc2 = this.serverSocket.accept();
				System.out.println("Cliente 2 accepted");
				Battleship game = new Battleship(sc1, sc2);
				game.start();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
}
