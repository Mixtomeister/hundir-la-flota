package model;

import java.net.Socket;

public class Player {
	private String nickname;
	private Socket sc;
	private int[][] map;
	private int[][] disparos;
	private int aciertos;
	
	public Player(Socket sc) {
		this.sc = sc;
		this.disparos = new int[10][10];
		this.aciertos = 0;
	}
	
	public int disparo(int x, int y) {
		int target = map[x][y];
		if(target != 0) {
			aciertos++;
		}
		return target;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public Socket getSc() {
		return sc;
	}

	public void setSc(Socket sc) {
		this.sc = sc;
	}

	public int[][] getMap() {
		return map;
	}

	public void setMap(int[][] map) {
		this.map = map;
	}

	public int getAciertos() {
		return aciertos;
	}
}
