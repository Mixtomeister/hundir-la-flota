package view;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class VistaTablero {
	private static Stage stage;
	private String tituloVentana;
	
	public VistaTablero(String tituloVentana) {
		this.tituloVentana = tituloVentana;
		loadLayout();
		stage.show();
	}
	
	private void loadLayout() {
        try {
        	FXMLLoader loader = new FXMLLoader();
            loader.setLocation(VistaTablero.class.getResource("Tablero.fxml"));
            AnchorPane pane = (AnchorPane) loader.load();
			Scene scene = new Scene(pane);
			stage = new Stage();
			stage.setScene(scene);
			stage.setTitle(tituloVentana);
			stage.setResizable(false);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Stage getStage() {
		return stage;
	}
}

