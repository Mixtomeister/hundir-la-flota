package view;

import javafx.scene.layout.Pane;

public class Celda {
	private int x;
	private int y;
	private Pane pane;
	
	public Celda(int x, int y, Pane pane) {
		this.x = x;
		this.y = y;
		this.pane = pane;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Pane getPane() {
		return pane;
	}

	public void setPane(Pane pane) {
		this.pane = pane;
	}
	
}
