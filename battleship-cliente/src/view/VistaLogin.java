package view;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class VistaLogin extends Application {
	private static Stage stage;

	private void loadLayout() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(VistaLogin.class.getResource("Login.fxml"));
			Scene scene = new Scene((VBox) loader.load());
			stage = new Stage();
			stage.setScene(scene);
			stage.setTitle("Login");
			stage.setResizable(false);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Stage getStage() {
		return stage;
	}
	
	public static void launchView() {
		launch();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		stage = primaryStage;
		loadLayout();
		stage.show();
	}
}