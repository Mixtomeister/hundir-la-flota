package view;

import java.util.HashMap;
import java.util.Stack;

import controller.Controller;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

public class TableroController {
	@FXML
	private GridPane tuMapa;
	@FXML
	private GridPane mapaRival;
	@FXML
	private Label lbl_status;
	
	private HashMap<Integer, Celda> panes;
	private Stack<Integer> clicked;
	private boolean turn;
	private Celda lastCelda;
	
	public void initialize() {
		clicked = new Stack<Integer>();
		Controller.getInstance().setTableroController(this);
		loadPanes();
	}
	
	public void loadMapa(int map[][]) {
		for (int i = 0; i < map.length; i++) {
			for (int a = 0; a < map[i].length; a++) {
				switch (map[i][a]) {
				case 5:
					setCell("#BF00FF", i, a);
					break;
				case 4:
					setCell("#FF8000", i, a);
					break;
				case 3:
					setCell("#FFFF00", i, a);
					break;
				case 2:
					setCell("#00FF00", i, a);
					break;
				case 1:
					setCell("#0EE7CE", i, a);
					break;

				default:
					break;
				}
			}
		}
	}
	
	public void setCell(String color, int x, int y) {
		Pane pane = new Pane();
		pane.setBackground(new Background(new BackgroundFill(Color.web(color), CornerRadii.EMPTY, Insets.EMPTY)));
		pane.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
		tuMapa.add(pane, x, y);
	}
	
	private void loadPanes() {
		panes = new HashMap<Integer, Celda>();
		for (int i = 0; i < 10; i++) {
			for (int a = 0; a < 10; a++) {
				Pane pane = new Pane();
				Celda cell = new Celda(i, a, pane);
				pane.setOnMouseClicked(e ->{
					Pane temp = (Pane) e.getSource();
					if(turn && !clicked.contains(temp.hashCode())) {
						lastCelda = panes.get(temp.hashCode());
						Controller.getInstance().sendShoot(lastCelda.getX(), lastCelda.getY());
						clicked.push(temp.hashCode());
						turn = false;
					}
				});
				panes.put(pane.hashCode(), cell);
				mapaRival.add(pane, i, a);
			}
		}
	}
	
	public void updateShoot(String color) {
		lastCelda.getPane().setBackground(new Background(new BackgroundFill(Color.web(color), CornerRadii.EMPTY, Insets.EMPTY)));
		lastCelda.getPane().setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
	}

	public void setTurn(boolean turn) {
		this.turn = turn;
	}

	public Label getLbl_status() {
		return lbl_status;
	}
}