package view;

import controller.Controller;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class LoginController {
	@FXML
	private TextField txt_nick;
	@FXML
	private TextField txt_ip;
	@FXML
	private TextField txt_gate;
	@FXML
	private Button btn_con;
	@FXML
	private Label lbl_status;
	
	public void initialize() {
		Controller.getInstance().setLoginController(this);
		Controller.getInstance().cargarDatosLogin();
		
		btn_con.setOnMouseClicked(e -> Controller.getInstance().startConection());
	}
	
	public void cargarDatos(String nick,  String ip, String gate) {
		txt_nick.setText(nick);
		txt_ip.setText(ip);
		txt_gate.setText(gate);
	}

	public TextField getTxt_nick() {
		return txt_nick;
	}

	public TextField getTxt_ip() {
		return txt_ip;
	}

	public TextField getTxt_gate() {
		return txt_gate;
	}

	public Label getLbl_status() {
		return lbl_status;
	}
}
