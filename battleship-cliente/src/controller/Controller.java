package controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import model.ClientAdmin;
import model.Config;
import model.DataInfo;
import view.LoginController;
import view.TableroController;
import view.VistaLogin;
import view.VistaTablero;

public class Controller {
	private static Controller instance = new Controller();
	private LoginController loginController;
	private TableroController tableroController;
	private VistaTablero vista;
	private Config config;
	private ClientAdmin client;

	public Controller() {
		this.config = new Config("data/config.json");
	}

	public void showLogin() {
		VistaLogin.launchView();
	}
	
	public void showTablero() {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				VistaLogin.getStage().hide();
				vista = new VistaTablero("Tablero");
			}
			
		});
	}
	
	public void loadTablero(int map[][]) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				tableroController.loadMapa(map);
			}
			
		});
	}
	
	public void cargarDatosLogin() {
		String nick = "";
		String ip = "";
		String gate = "";
		if (this.config.get("nickname") != null) {
			nick = this.config.get("nickname");
		}
		if (this.config.get("server_ip") != null) {
			ip = this.config.get("server_ip");
		}
		if (this.config.get("server_gate") != null) {
			gate = this.config.get("server_gate");
		}
		this.loginController.cargarDatos(nick, ip, gate);
	}
	
	public void startConection() {
		client = new ClientAdmin(config.get("server_ip"), Integer.parseInt(config.get("server_gate")));
		if (client.conectar()) {
			changeLoginStatus("Esperando otro jugador...");
			GameController game = new GameController(client, loginController.getTxt_nick().getText());
			game.start();
		}else {
			changeLoginStatus("Fallo al conectar con el servidor");
		}
	}
	
	public void changeLoginStatus(String status) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				loginController.getLbl_status().setText(status);
			}
			
		});
	}
	
	public void setTurn() {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				tableroController.setTurn(true);
				tableroController.getLbl_status().setText("Tu turno.");
			}
		});
	}
	
	public void sendShoot(int x, int y) {
		Gson gson = new GsonBuilder().create();
		DataInfo data = new DataInfo(0);
		data.set("x", String.valueOf(x));
		data.set("y", String.valueOf(y));
		String req = gson.toJson(data);
		client.sendRequest(req);
	}
	
	public void updateShoot(int target) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				if(target == 0) {
					tableroController.updateShoot("#0000FF");
					tableroController.getLbl_status().setText("Objetivo tocado.");
				}else {
					tableroController.updateShoot("#FF0000");
					tableroController.getLbl_status().setText("Disparo fallido.");
				}
			}
		});
	}
	
	public void updateShootme(int target, int x, int y) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				if(target == 0) {
					tableroController.setCell("#0000FF", x, y);
				}else {
					tableroController.setCell("#FF0000", x, y);
				}
			}
		});
	}
	
	public void end(int win) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				if(win == 1) {
					info("Fin", "Has ganado la partida");
				}else {
					info("Fin", "Has perdido la partida");
				}
				System.exit(0);
			}
		});
	}
	
	public void info(String title, String body) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle(title);
		alert.setHeaderText(null);
		alert.setContentText(body);
		alert.showAndWait();
	}
	
	public void error(String title, String body) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle(title);
		alert.setHeaderText(null);
		alert.setContentText(body);
		alert.showAndWait();
	}
	
	public void setLoginController(LoginController loginController) {
		this.loginController = loginController;
	}

	public void setTableroController(TableroController tableroController) {
		this.tableroController = tableroController;
	}

	public static Controller getInstance() {
		return instance;
	}
	
}
