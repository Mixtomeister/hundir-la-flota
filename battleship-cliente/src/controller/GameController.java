package controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import model.ClientAdmin;
import model.DataInfo;

public class GameController extends Thread {
	private ClientAdmin server;
	private String nick;
	
	private int idCliente;
	private int tablero[][];
	private boolean isGame;

	public GameController(ClientAdmin client, String nick) {
		this.server = client;
		this.nick = nick;
	}
	
	@Override
	public void run() {
		Gson gson = new GsonBuilder().create();
		idCliente = server.getNumClient();
		Controller.getInstance().changeLoginStatus("Conectado como cliente " + idCliente);
		server.setProfile(nick);
		tablero = server.getTablero();
		Controller.getInstance().showTablero();
		Controller.getInstance().loadTablero(tablero);
		isGame = true;
		while(isGame) {
			String req = server.response();
			DataInfo data = gson.fromJson(req.substring(0, req.indexOf("}}") + 2), DataInfo.class);
			switch (data.get("request")) {
			case "turn":
				Controller.getInstance().setTurn();
				break;
			case "shoot":
				Controller.getInstance().updateShoot(Integer.parseInt(data.get("target")));
				break;
			case "shootme":
				Controller.getInstance().updateShootme(Integer.parseInt(data.get("target")), Integer.parseInt(data.get("x")), Integer.parseInt(data.get("y")));
				break;
			case "win":
				Controller.getInstance().end(Integer.parseInt("win"));
				isGame = false;
				break;
			default:
				break;
			}
		}
	}
}
