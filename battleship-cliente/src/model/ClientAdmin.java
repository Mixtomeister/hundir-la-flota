package model;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ClientAdmin {
	private Socket socket;
	private String ipServer;
	private int gateServer;
	
	public ClientAdmin(String ip, int gate) {
		this.ipServer = ip;
		this.gateServer = gate;
		this.socket = new Socket();
	}
	
	public boolean conectar() {
		try {
			this.socket.connect(new InetSocketAddress(this.ipServer, this.gateServer));
			return true;
		}catch (Exception e) {
			return false;
		}
	}
	
	public String response() {
		char[] msg = new char[1000];
		try {
			new InputStreamReader(socket.getInputStream()).read(msg);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new String(msg);
	}
	
	public void sendRequest(String req) {
		try {
			socket.getOutputStream().write(req.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public int getNumClient() {
		Gson gson = new GsonBuilder().setLenient().create();
		String req = response();
		DataInfo data = gson.fromJson(req.substring(0, req.indexOf("}}") + 2), DataInfo.class);
		return Integer.parseInt(data.get("client"));
	}
	
	public void setProfile(String nickname) {
		Gson gson = new GsonBuilder().setLenient().create();
		DataInfo data = new DataInfo(1);
		data.set("response", "profile");
		data.set("nick", nickname);
		sendRequest(gson.toJson(data));
	}
	
	public int[][] getTablero() {
		Gson gson = new GsonBuilder().setLenient().create();
		String req = response();
		DataInfo data = gson.fromJson(req.substring(0, req.indexOf("}}") + 2), DataInfo.class);
		int[][] tablero = gson.fromJson(data.get("map"), int[][].class);
		return tablero;
	}
}
