package model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class Config {
	
	private HashMap<String,String> ini;
	private String file;
	
	public Config(String file) {
		this.file = file;
		this.load();
	}
	
	public void load() {
		Gson gson = new Gson();
		this.ini = new HashMap<String, String>();
		try {
			File f = new File(this.file);
			BufferedReader br = new BufferedReader(new FileReader(f));
			HashMap<String, String> temp = gson.fromJson(br, new TypeToken<HashMap<String, String>>(){}.getType());
			if(temp != null) {
				this.ini = temp;
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void save() {
		try {
			File f = new File(this.file);
			FileWriter fw = new FileWriter(f);
			Gson gb = new GsonBuilder().setPrettyPrinting().create();
			gb.toJson(this.ini, fw);
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String get(String key) {
		return this.ini.get(key);
	}
	
	public void put(String key, String value) {
		this.ini.put(key, value);
		save();
	}
}
