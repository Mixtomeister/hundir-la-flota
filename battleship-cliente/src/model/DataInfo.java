package model;

import java.util.HashMap;

public class DataInfo {
	private int type; // 0 Request, 1 Response
	private HashMap<String, String> parameters;
	
	public DataInfo(int type) {
		this.type = type;
		this.parameters = new HashMap<String, String>();
	}
	
	public void set(String key, String value) {
		this.parameters.put(key, value);
	}
	
	public String get(String key) {
		return this.parameters.get(key);
	}
}
